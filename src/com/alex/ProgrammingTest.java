package com.alex;

/**
 * 
 * @author Alejandro
 *
 * Some notes on the function:
 * - Assume 0 is a valid integer, but has no multiples
 * - numbers larger than parsable ints are ignored
 * - Palindromes are converted to uppercase in the output (the exercise says "capitalized", but the example only shows uppercase), 
 * 		this could be easily changed if needed.
 * - The largest multiple can be found in the middle of a word, not necessarily in the leftmost digits
 * - The exercise said to consider only alphanumeric characters, I assumed this references only the commas, which I remove from the input.
 */
public class ProgrammingTest {

	
	public String testFunction(int arg1, String arg2){
		
		// input validation
		if(arg2 == null || arg1< 0 || arg1>9 )
			return "invalid input";
		
		StringBuilder str = new StringBuilder();
		
		//clear commas
		for (int i = 0; i < arg2.length(); i++) {
			if(!(arg2.charAt(i) == ','))
				str.append(arg2.charAt(i));
		}
		
		//split into words
		String[] words = str.toString().split(" ");
		
		//clear str to store result
		str = new StringBuilder();
		
		//first pass for integers, only if input int is not 0
		if(arg1 != 0)
		for (int i = 0; i < words.length; i++) {
			try{
				Integer.parseInt(words[i]);
				str.append(processNumber(arg1, words[i]));
			}catch(NumberFormatException e){
				//do nothing
			}
		}

		//second pass for palindromes
		for (int i = 0; i < words.length; i++) {
			try{
				Integer.parseInt(words[i]);
			}catch(NumberFormatException e){
				str.append(processAlpha(words[i]));
			}
		}

		return str.toString();
	}
	
	
	private String processNumber(int a, String s){
		
		int max = 0;

		for(int j = 0; j <= s.length() -1; j++){
			for(int i = j+1; i <= s.length(); i++){
				//System.out.println(s.substring(j, i));
				int curr = Integer.parseInt(s.substring(j, i));

				if(curr%a == 0 && curr>max)
					max = curr;
			}
		}

		if(max > 0)
			return Integer.toString(max) + " ";
		else
			return "";
	}
	
	private String processAlpha(String s){
		
		StringBuilder sr = new StringBuilder(s);
		if(s.equalsIgnoreCase(sr.reverse().toString())){
			return s.toUpperCase() + " ";
		}
		return "";
	}
	
	
	public static void main(String[] args){
		ProgrammingTest test1 = new ProgrammingTest();
		System.out.println(test1.testFunction(8,"ANNA BAKES 80 CAKES IN THE NOON, 989216011"));
		System.out.println(test1.testFunction(0,"ANNA BAKES 80 CAKES IN THE NOON, 989216011"));
		System.out.println(test1.testFunction(10,"ANNA BAKES 80 CAKES IN THE NOON, 989216011"));
		System.out.println(test1.testFunction(1,"ANNA BAKES 80 CAKESekac IN THE NOON, 9892160112345347564567864571333"));
		System.out.println(test1.testFunction(2,"AN,NA BAKES 80 CAKES IN THE NOoON, 1011"));
		System.out.println(test1.testFunction(9,"ANNA BAKES 80 CAKES IN THE NOoON, 181127"));
		
	}
	
}
